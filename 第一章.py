#第一个注释
print('Hello,Wold!')#第二个注释
'''
这是长注释
第一行注释
第二行注释
'''
print('Hello,World!')
#过程讲究缩进，用缩进识别代码，以下是正确示例
if True:
    print("True")
else:
    print("False")
#长语句为了便于阅读用反斜杠\来换行
weekdays="Little Robert asked his mother for two cents.\
 'What did you do with the money I gave you yesterday?'"
print(weekdays)
#和用户交互，等待用户输入
print("Who are you?")
you=input()
print("Hello!")
print(you)
#变量赋值
a = 42
print(a)
#不同变量的类型观察
a=1
print(type(a))
b=3.14
print(type(b))
c=True
print(type(c))
#Python所支持的运算类型包括加法、减法、除法、整除、取余、乘法和乘方
print((3+1))
print((8.4-3))
print(15/4)
print(15//4)
print(15%4)
print(2*3)
print(2**3)
#用3个引号引用长文本，注意3个引号单独存在时是长文本注释
print('''Mike:Hi,How are you?
LiMing:Fine,Thank you!and you?
Mike:I'm fine, too! ''')
#引号里面用反斜杠作为转移符
print('what\'s your name?')
#文本前用r标记使得特殊字符失效，原样输出
print(r"Newlines are indicated by \n")
#字符串截取使用中括号
str='Lingyi'
print(str[0])
print(str[1:4])
print(str[-1])
#字符用加号+表示连接，数值类型要想转成文本
num=1
string='1'
num2=int(string)
print(num+num2)
#如果加号+两边是数值此时加号表示四则运算的加法
a=1
b=2
c=a+b
print(c)
#创建列表
list=["Python",12,[1,2,3],3.14,True]
print(list)
#列表切片从0开始，用中括号[]引用，删除列表用remove方法
list=[1,2,3,4]
print(list[0])
list.remove(3)
print(list)
#元组写在小括号里，创建后元组的元素不能被修改
tuple=['abc',(76,'ly'),898,5.2]
print(tuple[1:3])
#集合使用大括号{}创建
age={18,19,18,20,21,20}
print(age)
#创建字典，以及新增字典中的元素
information={
    'name':'liming',
    'age':'24'
}
print(information)
information['sex']='boy'
print(information)
#判断语句if结构
password = '12345'
if password == '12345':
    print('login sucess!')
else:
    print('wrong password')
#循环语句for结构
sum=0
for i in range(1,10,1):#不包含10，实际为1-9
    sum=i+sum
print(sum)
#自定义函数
def y(x):
    y=5*x+2
    return y
d=y(5)# 调用自定义函数y
print(d)